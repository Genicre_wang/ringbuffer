#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>
#include "ringbuffer.h"


/*
 * 环形缓冲区
 * 最多可以创建MAX_RINGBUFFER_NUM(10)个确定的ringbuffer_t类型的环形缓冲区
 */
    
#ifndef NULL
#define NULL (void*)0
#endif

#define IS_BD_LEGAL(x) (x >= 0 && x < MAX_RINGBUFFER_NUM)

struct ringbuffer_t
{
    int   buffer_description;  //环形缓冲区描述符
    void* buffer;              //环形缓冲区数组的地址
    int   each_item_len;       //每个数据的长度
    int   item_total_num;      //环形缓冲区存放数据的数量
    int   read_pos;            //写位置
    int   write_pos;           //读位置
    int   is_cover;            //是否可以覆盖
    int   item_exist_num;      //是否可以覆盖
};

static int assign_buffer_description(void);

/* 存有ringbuffer_t指针的数组 */
static struct ringbuffer_t* ringbuffer_arr[MAX_RINGBUFFER_NUM] = { NULL };

/* 分配描述符,无空间返回-1 */
static int assign_buffer_description(void)
{
    int index = 0;
    for (index = 0; index < MAX_RINGBUFFER_NUM; index++)
    {
        if (ringbuffer_arr[index] == NULL)
            return index;
    }
    return RINGBUFFER_BD_FULL;
}

int ringbuffer_creat(int item_num, int each_item_len, int iscover)
{
    int ringbuffer_len;

    /* 分配描述符，即结构体指针在数组中的位置 */
    int buffer_description = assign_buffer_description();
    
    if (IS_BD_LEGAL(buffer_description))
    {
        /* 初始化此ringbuf */
        ringbuffer_arr[buffer_description] = (struct ringbuffer_t*)malloc(sizeof(struct ringbuffer_t));
        ringbuffer_arr[buffer_description]->item_total_num = item_num;
        ringbuffer_arr[buffer_description]->each_item_len = each_item_len;
        ringbuffer_len = ringbuffer_arr[buffer_description]->item_total_num * ringbuffer_arr[buffer_description]->each_item_len;
        ringbuffer_arr[buffer_description]->buffer = (void*)malloc(ringbuffer_len);
        ringbuffer_arr[buffer_description]->read_pos = 0;
        ringbuffer_arr[buffer_description]->write_pos = 0;
        ringbuffer_arr[buffer_description]->is_cover = iscover;
        ringbuffer_arr[buffer_description]->item_exist_num = 0;
    }
    
    return buffer_description;
}

int ringbuffer_delete(int bd)
{
    if (!IS_BD_LEGAL(bd) || ringbuffer_arr[bd] == NULL)
    {
        printf("ringbuffer_delete, ringbuffer_arr[%d] is not exist\n", bd);
        return RINGBUFFER_PARAMETER_LEGAL;
    }
    
    free((void*)ringbuffer_arr[bd]->buffer);
    free((void*)ringbuffer_arr[bd]);
    ringbuffer_arr[bd] = NULL;
    
    return RINGBUFFER_OK;
}

int ringbuffer_write(int bd, void* data)
{
    if (!IS_BD_LEGAL(bd) || ringbuffer_arr[bd] == NULL)
    {
        printf("ringbuffer_write, ringbuffer_arr[%d] is not exist\n", bd);
        return RINGBUFFER_PARAMETER_LEGAL;
    }
    
    if (ringbuffer_arr[bd]->item_exist_num < ringbuffer_arr[bd]->item_total_num)
    {
        void* pos = (void*)(ringbuffer_arr[bd]->buffer + (ptrdiff_t)(ringbuffer_arr[bd]->write_pos * ringbuffer_arr[bd]->each_item_len));
        memcpy(pos, (void*)data, ringbuffer_arr[bd]->each_item_len);
        ringbuffer_arr[bd]->write_pos = (ringbuffer_arr[bd]->write_pos + 1) % ringbuffer_arr[bd]->item_total_num;
        ringbuffer_arr[bd]->item_exist_num++;
        return RINGBUFFER_OK;
    }
    else if (ringbuffer_arr[bd]->is_cover)
    {
        void* pos = (void*)(ringbuffer_arr[bd]->buffer + (ptrdiff_t)(ringbuffer_arr[bd]->write_pos * ringbuffer_arr[bd]->each_item_len));
        memcpy(pos, (void*)data, ringbuffer_arr[bd]->each_item_len);
        ringbuffer_arr[bd]->write_pos = (ringbuffer_arr[bd]->write_pos + 1) % ringbuffer_arr[bd]->item_total_num;
        ringbuffer_arr[bd]->read_pos = (ringbuffer_arr[bd]->read_pos + 1) % ringbuffer_arr[bd]->item_total_num;
        /* ringbuffer_arr[bd]->item_exist_num do not need increase */
    }
    
    return RINGBUFFER_FULL;
}

int ringbuffer_read(int bd, void* data)
{
    if (!IS_BD_LEGAL(bd) || ringbuffer_arr[bd] == NULL)
    {
        printf("ringbuffer_read, ringbuffer_arr[%d] is not exist\n", bd);
        return RINGBUFFER_PARAMETER_LEGAL;
    }
    
    if (ringbuffer_arr[bd]->item_exist_num)
    {
        void* pos = (void*)(ringbuffer_arr[bd]->buffer + (ptrdiff_t)(ringbuffer_arr[bd]->read_pos * ringbuffer_arr[bd]->each_item_len));
        memcpy((void*)data, pos, ringbuffer_arr[bd]->each_item_len);
        ringbuffer_arr[bd]->read_pos = (ringbuffer_arr[bd]->read_pos + 1) % ringbuffer_arr[bd]->item_total_num;
        ringbuffer_arr[bd]->item_exist_num--;
        return RINGBUFFER_OK;
    }
    
    return RINGBUFFER_EMPTY;
}

int ringbuffer_write_items(int bd, void* data, int size)
{
    int len = 0;
    void* pos = data;
    if (!IS_BD_LEGAL(bd) || ringbuffer_arr[bd] == NULL || size == 0)
    {
        printf("ringbuffer_write_items, ringbuffer_arr[%d] is not exist\n", bd);
        return RINGBUFFER_PARAMETER_LEGAL;
    }
    
    for(len = 0; len < size; len++)

    {
		if(ringbuffer_write(bd, pos) != 0)    break;
        pos += (ptrdiff_t)ringbuffer_arr[bd]->each_item_len;
    } 
    
    return len;
}

int ringbuffer_read_items(int bd, void* data, int size)
{
    int len = 0;
    void* pos = data;
    if (!IS_BD_LEGAL(bd) || ringbuffer_arr[bd] == NULL || size == 0)
    {
        printf("ringbuffer_read_items, ringbuffer_arr[%d] is not exist\n", bd);
        return RINGBUFFER_PARAMETER_LEGAL;
    }
    
    for(len = 0; len < size; len++)
    {
		if(ringbuffer_read(bd, pos) != 0)    break;
        pos += (ptrdiff_t)ringbuffer_arr[bd]->each_item_len;
    } 
    
    return len;
}

int ringbuffer_clean(int bd)
{
    if (!IS_BD_LEGAL(bd) || ringbuffer_arr[bd] == NULL)
    {
        printf("ringbuffer_clean, ringbuffer_arr[%d] is not exist\n", bd);
        return RINGBUFFER_PARAMETER_LEGAL;
    }

    ringbuffer_arr[bd]->read_pos  = 0;
    ringbuffer_arr[bd]->write_pos = 0;
    ringbuffer_arr[bd]->item_exist_num = 0;
    
    return RINGBUFFER_OK;
}

int ringbuffer_get_write_num(int bd)
{
    if (!IS_BD_LEGAL(bd) || ringbuffer_arr[bd] == NULL)
    {
        printf("ringbuffer_get_write_num, ringbuffer_arr[%d] is not exist\n", bd);
        return RINGBUFFER_PARAMETER_LEGAL;
    }

    return ringbuffer_arr[bd]->item_exist_num;
}


