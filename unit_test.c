#include <stdlib.h>
#include <stdio.h>
#include "ringbuffer.h"
struct _test{
    char a;
    char b;
    float c;
    int d;
}A,B,C;
int main()
{
	int BufferDescription0, BufferDescription1, BufferDescription2, bd;
	double a, b, c;
    int arr[3] = {1,2,3}, read_arr[3] = {0};
	int err, len;
	int index;
	/* crete RingBuffer*/
	printf("ringbuffer_creat & ringbuffer_delete test\n");
	BufferDescription0 = ringbuffer_creat(2, sizeof(char), cover);	printf("ringbuffer_creat, bd = %d\n", BufferDescription0);
	BufferDescription1 = ringbuffer_creat(2, sizeof(int), cover);	printf("ringbuffer_creat, bd = %d\n", BufferDescription1);
	/*delete and crete RingBuffer*/
	err = ringbuffer_delete(BufferDescription0);                    printf("ringbuffer_delete, bd = %d, err = %d\n", BufferDescription0, err);
	BufferDescription2 = ringbuffer_creat(2, sizeof(int*), cover);	printf("ringbuffer_creat, bd = %d\n", BufferDescription2);
	BufferDescription0 = ringbuffer_creat(2, sizeof(int), cover);	printf("ringbuffer_creat, bd = %d\n", BufferDescription0);

	printf("\nringbuffer full test\n");
	/* bd(10)&err(10) = -1 */
	for (index = 3; index < 11; index++)
	{
 		bd = ringbuffer_creat(2, sizeof(int), cover); printf("ringbuffer_creat, bd = %d\n", bd);
 	}
    
	printf("\nbd err test\n");
	for (index = 0; index < 11; index++)
	{
		err = ringbuffer_delete(index);  printf("ringbuffer_delete, bd = %d, err = %d\n", index, err);
	}

	/*uncover write data */
	printf("\nuncover read/write data\n");
    a = 1; b = 2, c = 3;
	bd = ringbuffer_creat(2, sizeof(a), uncover);
	err = ringbuffer_write(bd, &a);	printf("write %d, save_data_num = %d, err = %d\n", (int)a, ringbuffer_get_write_num(bd), err);
	err = ringbuffer_write(bd, &b);	printf("write %d, save_data_num = %d, err = %d\n", (int)b, ringbuffer_get_write_num(bd), err);
	err = ringbuffer_write(bd, &c); printf("write %d, save_data_num = %d, err = %d\n", (int)c, ringbuffer_get_write_num(bd), err);
	/* get data */
    //ringbuffer_clean(bd);
	err = ringbuffer_read(bd, &c);	printf("read = %d, save_data_num = %d, err = %d\n", (int)c, ringbuffer_get_write_num(bd), err);
	err = ringbuffer_read(bd, &c);	printf("read = %d, save_data_num = %d, err = %d\n", (int)c, ringbuffer_get_write_num(bd), err);
	err = ringbuffer_read(bd, &c);  printf("read = %d, save_data_num = %d, err = %d\n", (int)c, ringbuffer_get_write_num(bd), err);
    ringbuffer_delete(bd);

    /*cover write data */
	printf("\ncover read/write data\n");
    a = 1; b = 2, c = 3;
	bd = ringbuffer_creat(2, sizeof(a), cover);
	err = ringbuffer_write(bd, &a);	printf("write %d, save_data_num = %d, err = %d\n", (int)a, ringbuffer_get_write_num(bd), err);
	err = ringbuffer_write(bd, &b);	printf("write %d, save_data_num = %d, err = %d\n", (int)b, ringbuffer_get_write_num(bd), err);
	err = ringbuffer_write(bd, &c); printf("write %d, save_data_num = %d, err = %d\n", (int)c, ringbuffer_get_write_num(bd), err);
	/* get data */
	err = ringbuffer_read(bd, &c);	printf("read = %d, save_data_num = %d, err = %d\n", (int)c, ringbuffer_get_write_num(bd), err);
	err = ringbuffer_read(bd, &c);	printf("read = %d, save_data_num = %d, err = %d\n", (int)c, ringbuffer_get_write_num(bd), err);
	err = ringbuffer_read(bd, &c);  printf("read = %d, save_data_num = %d, err = %d\n", (int)c, ringbuffer_get_write_num(bd), err);
    ringbuffer_delete(bd);
    
	/*uncover write struct */
	printf("\nuncover read/write struct\n");
    A.d = 1; B.d = 2, C.d = 3;
	bd = ringbuffer_creat(2, sizeof(A), uncover);
	err = ringbuffer_write(bd, &A);	printf("write %d, save_data_num = %d, err = %d\n", A.d, ringbuffer_get_write_num(bd), err);
	err = ringbuffer_write(bd, &B);	printf("write %d, save_data_num = %d, err = %d\n", B.d, ringbuffer_get_write_num(bd), err);
	err = ringbuffer_write(bd, &C); printf("write %d, save_data_num = %d, err = %d\n", C.d, ringbuffer_get_write_num(bd), err);
	/* get data */
	err = ringbuffer_read(bd, &C);	printf("read = %d, save_data_num = %d, err = %d\n", C.d, ringbuffer_get_write_num(bd), err);
	err = ringbuffer_read(bd, &C);	printf("read = %d, save_data_num = %d, err = %d\n", C.d, ringbuffer_get_write_num(bd), err);
	err = ringbuffer_read(bd, &C);  printf("read = %d, save_data_num = %d, err = %d\n", C.d, ringbuffer_get_write_num(bd), err);
    ringbuffer_delete(bd);

	printf("\nuncover read/write datas\n");
	bd = ringbuffer_creat(2, sizeof(int), cover);
    len = ringbuffer_write_items(bd, arr, 3);
    printf("write len =  %d, save_data_num = %d\n", len, ringbuffer_get_write_num(bd));
    len = ringbuffer_read_items(bd, read_arr, 3);
    printf("read len =  %d, save_data_num = %d\n", len, ringbuffer_get_write_num(bd));
    printf("arr %d,%d,%d-- read_arr %d,%d,%d\n",arr[0], arr[1], arr[2],read_arr[0], read_arr[1], read_arr[2]);
    ringbuffer_delete(bd);
}


