
.PHONY : all clean

all:
	@gcc unit_test.c ringbuffer.c -o unit_test.out
	@echo "build"
clean:
	@rm -f unit_test.out