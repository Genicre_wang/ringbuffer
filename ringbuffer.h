#ifndef _RINGBUFFER_
#define _RINGBUFFER_

/* 可以创建的最大缓冲区的个数 */
#define MAX_RINGBUFFER_NUM          (10)
#define RINGBUFFER_OK               (0)
#define RINGBUFFER_PARAMETER_LEGAL  (-1)
#define RINGBUFFER_EMPTY            (-2)
#define RINGBUFFER_FULL             (-3)
#define RINGBUFFER_BD_FULL          (-4)

/* ringbuffer_creat的最后一个参数 */
enum iscover_t
{
    uncover = 0,
    cover   = 1
};
    
/**
  *  @brief               此函数会创建一个ringbuffer，并返回描述符
  *  @param item_num      ringbuffer中存入数据的最大个数
  *  @param each_item_len ringbuffer中每个数据占用内存的字节数
  *  @param iscover       是否覆盖写入
  *  @Sample usage: 
 ***/
int ringbuffer_creat(int item_num, int each_item_len, int iscover);

/**
  *  @brief     删除一个ringbuffer
  *  @param bd  ringbuffer的描述符
  *  @Sample usage: 
 ***/
int ringbuffer_delete(int bd);

/**
  *  @brief      向ringbuffer中写入一个数据
  *  @param bd   ringbuffer的描述符
  *  @param data 数据的地址  
  *  @Sample usage: 
 ***/
int ringbuffer_write(int bd, void* data);

/**
  *  @brief      从ringbuffer中读取一个数据
  *  @param bd   ringbuffer的描述符
  *  @param data 数据的地址  
  *  @Sample usage: 
 ***/
int ringbuffer_read(int bd, void *data);

/**
  *  @brief      向ringbuffer中写入一个数据
  *  @param bd   ringbuffer的描述符
  *  @param data 数据的地址
  *  @param size 数据的个数
  *  @Sample usage: 
 ***/
int ringbuffer_write_items(int bd, void* data, int size);

/**
  *  @brief      从ringbuffer中读取多个数据
  *  @param bd   ringbuffer的描述符
  *  @param data 数据的地址
  *  @param size 数据的个数  
  *  @Sample usage: 
 ***/
int ringbuffer_read_items(int bd, void* data, int size);

/**
  *  @brief      清空ringbuffer
  *  @param bd   ringbuffer的描述符
  *  @Sample usage: 
 ***/
int ringbuffer_clean(int bd);

/**
  *  @brief      得到ringbuffer中已存入的数据数量
  *  @param bd   ringbuffer的描述符
  *  @Sample usage: 
 ***/
int ringbuffer_get_write_num(int bd);

#endif





